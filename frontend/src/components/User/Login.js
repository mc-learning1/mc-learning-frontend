import React, { useState, useRef, useEffect } from "react";
import { useNavigate, Form } from "react-router-dom";

const USER_REGEX = /[a-zA-Z][a-zA-Z ]+[a-zA-Z]$/ ;

export const Login = () => {
  const navigate = useNavigate();
  const userRef = useRef();
  const errRef = useRef();

  const [errMsg, setErrMsg] = useState("");
  const [userFocus, setUserFocus] = useState(false);
  const [validName, setValidName] = useState(false);

  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  const [authenticated, setauthenticated] = useState(
    localStorage.getItem(localStorage.getItem("authenticated") || false)
  );
  
  const [error, setError] = useState();

  const routeChange = (e) => {
    e.preventDefault();
    let path = `register`;
    navigate(path);
  };

  useEffect(() => {
    userRef.current.focus();
  }, []);

  useEffect(() => {
    setValidName(USER_REGEX.test(userName));
  }, [userName]);

  useEffect(() => {
    setErrMsg("");
  }, [userName]);

  const handleSubmit = (e) => {
    e.preventDefault();
    const v1 = USER_REGEX.test(userName);
    if (!v1) {
      setErrMsg("Invalid User Name");
      return;
    } 
    
    fetch("http://localhost:8080/customer/login/", {
      method: "POST",
      body: JSON.stringify({
        userName: userName,
        password: password,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        crossorigin: "true",
        mode: "no-cors",
      },
    })
      .then((response) => {
        if (response.status === 200) {
          navigate("/dashboard");
        } else {
          // navigate("/login");
          setErrMsg("Invalid Credentials!!!");
        }
      })
      .then((data) => {
        console.log(data);
        // Handle data
      })
      .catch((err) => {
        console.log(err.message);
        setError("Something went wrong!!" + err.message);
      });
  };

  return (
    <div className="auth-form-container">
      <h2>Login</h2>
      <p
        ref={errRef}
        className={errMsg ? "errmsg" : "offscreen"}
        aria-live="assertive"
      >
        {errMsg}
      </p>
      <form className="login-form" onSubmit={handleSubmit}>
        <label htmlFor="username">User Name</label>
        <input
          required
          ref={userRef}
          onFocus={() => setUserFocus(true)}
          value={userName}
          onChange={(e) => setUserName(e.target.value)}
          type="text"
          placeholder="Enter your username"
          id="userName"
          name="userName"
        />

        <label htmlFor="password">password</label>
        <input
          required
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          placeholder="********"
          id="password"
          name="password"
        />

        <button type="submit">Log In</button>
      </form>
        <button className="link-btn" onClick={routeChange}>
        Don't have an account? Register here.
      </button>
      {error ? <p>{error}</p> : null}
    </div>
  );
};

export default Login;
