import React from "react";

const PersonalHomeLoan = () => {
  return (
    <div>
      {/*      <p>Personal/Home Loan Section</p> */}
      <form className="register-form">
      <label htmlFor="annualincome">Annual Income</label>
            <input type="number" name="annualincome" id="annualincome" placeholder="Enter Annual Income" />
            <label htmlFor="companyname">Company Name</label>
            <input type="text" name="companyname" id="companyname" placeholder="Enter Company Name" />
            <label htmlFor="designation">Designation</label>
            <input type="text" name="designation" id="designation" placeholder="Enter Designation" />
            <label htmlFor="totalexp">Total Experience</label>
            <input type="number" name="totalexp" id="totalexp" placeholder="Enter Total Experience" />
            <label htmlFor="currentexp">Experience with Current Company</label>
            <input type="number" name="currentexp" id="currentexp" placeholder="Enter Experience with Current Company" />
            
      </form>
          
    </div>
  );
};
export default PersonalHomeLoan;
