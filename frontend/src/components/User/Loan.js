import React, { useState} from "react";
import DatePicker from 'react-date-picker';
import EducationLoan from './Educationloan';
import PersonalHomeLoan from './Personalhomeloan';


const Loan = () => {
  const [startDate, setStartDate] = useState(new Date());
  const options= [
    {
      name: 'Select',
      value: null,
    },
    {
      name: 'EducationLoan',
      value: 'Education Loan',
    },
    {
      name: 'Personal/HomeLoan',
      value: 'Personal/Home Loan',
    }]
  const [selectedOption, setSelectedOption] = useState(options[0].value);
  
  return (
    <div>  
      <form className="register-form">    
            <h1>Loan Page</h1>  
        <label> Select Loan Type:</label>
        <select
        value={selectedOption}
        onChange={e => setSelectedOption(e.target.value)}>
        {options.map(o => (
          <option key={o.value} value={o.value}>{o.name}</option>
        ))}
      </select>
{
  selectedOption==='Personal/Home Loan' && 
  <PersonalHomeLoan/>
  }

{
  selectedOption==='Education Loan' &&   
    <EducationLoan/>
  
 }          <label>Loan Amount:</label>
          <input type="number" placeholder="Enter required loan amt" id="loanAmount" name="loanAmount" />
          <label>Loan Apply Date:</label>
          <DatePicker showIcon selected={startDate} onChange={date => setStartDate(date)} />
          <label>Rate Of Interest:</label>
          <input type="number" placeholder="Enter interest percentage" id="rateOfInterest" name="rateOfInterest" />
          <label>Duration of the loan</label>
          <input type="number" placeholder="Enter number of years" id="loanDuration" name="loanDuration" />
        <button type="submit">Submit</button>
    </form>
    </div>
    
  );
};
export default Loan;

