import React from "react";

const Account = () => {
  return (
    <div>
             <form className="register-form">
      <label htmlFor="accounttype">Account Type</label>
            <input type="text" name="accounttype" id="accounttype" placeholder="Enter Account Type" />
            <label htmlFor="depositamt">Deposit Amount</label>
            <input type="number" name="depositamount" id="depositamount" placeholder="Enter amount" />
            <button type="submit">Deposit</button>
      </form>
          
    </div>
  );
};
export default Account;
