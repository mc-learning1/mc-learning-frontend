import React from "react";
import { useNavigate, Link } from "react-router-dom";

export const Dashboard =  () => {
    
    const navigate = useNavigate;
    
    function routeLoanPage(e){ 
       
        e.preventDefault();
        let path = `loan`; 
        navigate(path);
      }
      
  const routeAccountPage = (e) =>{ 
        e.preventDefault();
          /*  let path = `accountdetails`; 
        navigate(path);*/
      }
      
  return (
    <div>
      <div className="auth-form-container">
            <h2>Dash Board</h2>
           
           <Link className="link-btn" to="/loan">Apply Loan here here.</Link>
           <Link className="link-btn" to="/accountdetails">View/Update your Account Details.</Link>
            
           {/* <button className="link-btn" onClick={routeLoanPage}>Apply Loan here here.</button>
            <button className="link-btn" onClick={routeAccountPage}>View/Update your Account Details.</button>
  */}
        </div>
          
    </div>
  );
};
export default Dashboard;
