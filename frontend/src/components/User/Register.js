import React, { useState, useRef, useEffect } from "react";
import { useNavigate, redirect } from "react-router-dom";

const USER_REGEX = /[a-zA-Z][a-zA-Z ]+[a-zA-Z]$/;
const PHONE_REGEX = /^[0-9]{10}$/;
const EMAIL_REGEX =/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;


export const Register = () => {
  const userRef = useRef();
  const errRef = useRef();

  const [name, setName] = useState("");
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [address, setAddress] = useState("");
  const [state, setState] = useState("");
  const [country, setCountry] = useState("");
  const [emailAddress, setEmailAddress] = useState("");
  const [pan, setPan] = useState("");
  const [contactNo, setContactNo] = useState("");
  const [dateOfBirth, setDateOfBirth] = useState("");
  /*const [branch, setBranch] = useState('');
    const [acctype, setAcctype] = useState('');
    const [depositamt, setDepositAmt] = useState('');
   */

  const [error, setError] = useState();
  const [validName, setValidName] = useState(false);
  const [errMsg, setErrMsg] = useState(false);
  
  useEffect(() => {
    //userRef.current.focus();
  }, []);

  useEffect(() => {
    setValidName(USER_REGEX.test(userName));
    setValidName(PHONE_REGEX.test(contactNo));
    setValidName(EMAIL_REGEX.test(emailAddress));
  }, [userName,contactNo,emailAddress]);

  useEffect(() => {
    setErrMsg("");
  }, [userName,contactNo,emailAddress]);

  const navigate = useNavigate();

  const routeLoginPage = () => {
    navigate("/");
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const v1 = USER_REGEX.test(userName);
    if (!v1) {
      setErrMsg("Invalid User Name");
      return;
    }
    
    const v2 = PHONE_REGEX.test(contactNo);
    if (!v2) {
      setErrMsg("Invalid Contact No. Contact number should be 10 digits");
      return;
    }
    
    const v3 = EMAIL_REGEX.test(emailAddress);
    if (!v3) {
      setErrMsg("Invalid Email-ID!");
      return;
    }

    fetch("http://localhost:8080/customer/register/", {
      method: "POST",
      body: JSON.stringify({
        name: name,
        userName: userName,
        password: password,
        address: address,
        state: state,
        country: country,
        emailAddress: emailAddress,
        pan: pan,
        contactNo: contactNo,
        dateOfBirth: dateOfBirth,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        crossorigin: "true",
        mode: "no-cors",
      },
    })
      .then((response) => {
        if (response.status === 200) {
          //navigate("/dashboard");
          setErrMsg("Customer registered successfully!!");
        } else {
          // navigate("/login");
          //setErrMsg("Invalid Credentials!!!");
        }
      })
      .then((data) => {
        console.log(data);
        // Handle data
      })
      .catch((err) => {
        console.log(err.message);
        setErrMsg("Something went wrong!!" + err.message);
      });
  };

  return (
    <div className="auth-form-container">
      <h2>Register</h2>
     
      <form className="register-form" onSubmit={handleSubmit}>
        <label htmlFor="name">Full name</label>
        <input
          required
          value={name}
          name="name"
          onChange={(e) => setName(e.target.value)}
          id="name"
          placeholder="full Name"
        />

        <label htmlFor="userName">User Name</label>
        <input
          required
          value={userName}
          onChange={(e) => setUserName(e.target.value)}
          type="text"
          placeholder="Enter your username"
          id="userName"
          name="userName"
        />

        <label htmlFor="password">password</label>
        <input
          required
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          placeholder="********"
          id="password"
          name="password"
        />

        <label htmlFor="address">Address</label>
        <input
          required
          value={address}
          name="address"
          onChange={(e) => setAddress(e.target.value)}
          id="address"
          placeholder="address"
        />

        <label htmlFor="state">State</label>
        <input
          required
          value={state}
          name="state"
          onChange={(e) => setState(e.target.value)}
          id="state"
          placeholder="state"
        />

        <label htmlFor="country">Country</label>
        <input
          required
          value={country}
          name="country"
          onChange={(e) => setCountry(e.target.value)}
          id="country"
          placeholder="country"
        />

        <label htmlFor="emailAddress">email</label>
        <input
          required
          value={emailAddress}
          onChange={(e) => setEmailAddress(e.target.value)}
          type="emailAddress"
          placeholder="youremail@gmail.com"
          id="emailAddress"
          name="emailAddress"
        />

        <label htmlFor="country">Country</label>
        <input
          required
          value={country}
          name="country"
          onChange={(e) => setCountry(e.target.value)}
          id="country"
          placeholder="country"
        />
     
        
        <label htmlFor="pan">PAN No.</label>
        <input
          required
          value={pan}
          onChange={(e) => setPan(e.target.value)}
          type="pan"
          placeholder="Pan"
          id="pan"
          name="pan"
        />
        
        
        < label htmlFor="contactNo">Contact No</label>
        <input
          required
          value={contactNo}
          onChange={(e) => setContactNo(e.target.value)}
          type="contactNo"
          placeholder="contactNo"
          id="contactNo"
          name="contactNo"
        />
        
        <label htmlFor="dateOfBirth">Date of Birth</label>
        <input
          required
          value={dateOfBirth}
          type="date"          
          name="dateOfBirth"
          onChange={(e) => setDateOfBirth(e.target.value)}
          id="dateOfBirth"
          placeholder="Date of Birth"
        />

        {/*   <label htmlFor="acctype">Account type</label>
            <input value={acctype} name="acctype" onChange={(e) => setAcctype(e.target.value)} id="acctype" placeholder="" />
            
            <label htmlFor="branch">Branch Name</label>
            <input value={branch} name="branch" onChange={(e) => setBranch(e.target.value)} id="branch" placeholder="Bracnch" />
            
            <label htmlFor="depositamt">Initial Deposit Amount</label>
            <input value={depositamt} name="depositamt" onChange={(e) => setDepositAmt(e.target.value)} id="depositamt" placeholder="Amount" />
    */}

        <button type="submit">Register</button>
        
        
      </form>
      
      <p
        ref={errRef}
        className={errMsg ? "errmsg" : "offscreen"}
        aria-live="assertive"
      >
        {errMsg}
      </p>
      
      {/*<button className="link-btn" onClick={() => props.onFormSwitch('login')}>Already have an account? Login here.</button>*/}
      <button className="link-btn" onClick={routeLoginPage}>
        Already have an account? Login here.
      </button>
    </div>
  );
};

export default Register;
