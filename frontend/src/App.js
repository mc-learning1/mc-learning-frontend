
import React, { useState } from "react";
import { Route, BrowserRouter as Router,Routes} from 'react-router-dom';
import { Login } from "./components/User/Login";
import { Register } from "./components/User/Register";

import './App.css';
import Dashboard from "./components/User/Dashboard";
import Loan from "./components/User/Loan";
import Account from "./components/User/Account";



const App = () => {
  const [currentForm, setCurrentForm] = useState('login');

  const toggleForm = (formName) => {
    setCurrentForm(formName);
  }

  return (
    
    
    <div className="App">
      
    <Router>
      <Routes>
        
         <Route path="/" element={<Login />} />
         <Route path="register" element={<Register />} />
         <Route path="dashboard" element={<Dashboard />} />
         <Route path="loan" element={<Loan />} />
         <Route path="accountdetails" element={<Account/>} />
        
      </Routes>
      </Router>
      {
       // currentForm === "login" ? <Login onFormSwitch={toggleForm} /> : <Register onFormSwitch={toggleForm} />
      }
       
    </div>
  
  );
  }
export default App;
    